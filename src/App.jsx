import { Provider } from "react-redux"
import { store } from "@store/store"

import { TangramMap } from "@components/Map"
import Hud from "@components/hud/Hud"
import GoToSourceButton from "@components/hud/GoToSourceButton"

import "@styles/App.css"
import PaperFraming from "@components/PaperFraming"

function App() {
    return (
        <Provider store={store}>
            <div className="w-full h-full relative">
                <PaperFraming>
                    <TangramMap />
                </PaperFraming>
                <Hud />
                <GoToSourceButton />
            </div>
        </Provider>
    )
}

export default App
