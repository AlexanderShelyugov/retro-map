import React, { useRef, useEffect } from "react"
import L from "leaflet"
import tangram from "tangram"

export const TangramMap = () => {
    const mapContainer = useRef()

    useEffect(() => {
        if (!mapContainer.current) {
            return
        }
        const mapConfig = {
            scene: "scene.yaml",

            view: {
                lat: 40.70531887544228,
                lng: -74.00976419448853,
                zoom: 16,
            },
        }

        const map = L.map(mapContainer.current, { zoomControl: false }).setView(
            [mapConfig.view.lat, mapConfig.view.lng],
            mapConfig.view.zoom
        )

        const tangramLayer = tangram.leafletLayer(mapConfig)
        tangramLayer.addTo(map)

        return () => {
            if (map) {
                map.remove()
            }
        }
    }, [])

    return <div
        className="w-full h-full bg-[#F2F2EA]"
        style={{
            border: "#F2F2EA 2px solid"
        }}
        tabIndex="-1"
        ref={mapContainer}
        />
}
