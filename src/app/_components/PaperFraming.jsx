export default ({ children }) => (
    <div
        className="w-full h-full bg-repeat p-[12mm]"
        style={{
            backgroundImage: "url(/images/paper-texture.jpg)"
        }}
    >
        {children}
    </div>
)
