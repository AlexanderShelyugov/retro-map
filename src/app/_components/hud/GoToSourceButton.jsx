export default () => (
    <div
        className="w-[20mm] h-[20mm] absolute z-[400] rounded-full duration-300 rotate-[25deg]
        border drop-shadow hover:drop-shadow-xl paper-clip visit-source top-[25mm] right-[4mm] hover:scale-125 hover:rotate-6"
        style={{
            borderColor: "#535049",
        }}
    >
        <a href="https://gitlab.com/AlexanderShelyugov/retro-map" target="_blank" rel="noopener noreferrer">
            <img src="images/gitlab-logo-550.png" alt="Visit source code repository" title="Visit source code" />
        </a>
    </div>
)
