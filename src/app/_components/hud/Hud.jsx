import LocationSearchResults from "@components/hud/LocationSearchResults"
import SearchForm from "@components/hud/SearchForm"

export default () => (
    <aside className="absolute top-0 left-0 bottom-0 w-96 flex flex-col p-2 pointer-events-auto z-400">
        <SearchForm />
        <LocationSearchResults />
    </aside>
)
