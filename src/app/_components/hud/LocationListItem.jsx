export default ({ place }) => {
    const showAlert = (e) => alert("Clickd")

    return (
        <div className="h-24 p-2 text-slate-500 hover:text-slate-200 cursor-pointer truncate text-lg typewriter duration-300 ease-in-out" onClick={showAlert}>
            {place.location}
        </div>
    )
}
