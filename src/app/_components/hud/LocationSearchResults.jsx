import { useSelector } from "react-redux"
import LocationListItem from "@components/hud/LocationListItem"

export default () => {
    const places = useSelector((state) => {
        return state.locationResults.places
    })

    if (!places || places.length <= 0) return null

    const placeViews = places.map((place) => <LocationListItem key={place.id} place={place} />)

    return (
        <div className="relative h-full pt-8 pd-4 overflow-y-auto divide-y divide-slate-500 paper-clip drop-shadow">
            {placeViews}
        </div>
    )
}
