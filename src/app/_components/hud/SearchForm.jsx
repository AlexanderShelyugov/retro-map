import { useDispatch } from "react-redux"
import debounce from "debounce"
import { useLazySearchByQuery } from "@store/features/search/NominatimClient"

export default () => {
    const dispatch = useDispatch()
    const [searchByQuery] = useLazySearchByQuery()
    let onQueryInput = debounce((e) => {
        const query = e.target.value
        searchByQuery(query)
    }, 500)

    return (
        <div className="relative -top-6 -left-6 pt-8 pl-8 pr-6 pb-10 flex justify-center items-center gap-2 drop-shadow-lg paper-clip">
            <h3 className="typewriter text-xl">Search</h3>
            <input type="text" className="w-full text-lg dotted-underline" onChange={onQueryInput} />
        </div>
    )
}
