export const metadata = {
    title: "Retro Map",
    description: "Retro Map",
}

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <body>
                <noscript>You need to enable JavaScript to run this app.</noscript>
                <div id="root">{children}</div>
            </body>
        </html>
    )
}
