import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"

export const nominatimClient = createApi({
    reducerPath: "nominatim",
    baseQuery: fetchBaseQuery({ baseUrl: "https://nominatim.openstreetmap.org" }),
    endpoints: (builder) => ({
        searchBy: builder.query({
            query: (queryInputText) => ({
                url: `search?q=${queryInputText}&format=json&polygon_kml=1`,
                method: "GET",
            })
        }),
    }),
})

export const { useSearchByQuery, useLazySearchByQuery } = nominatimClient
