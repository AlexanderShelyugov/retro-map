import { createSlice } from '@reduxjs/toolkit'
import { nominatimClient, useSearchByQuery } from '@store/features/search/NominatimClient'

const initialState = {
    places: []
}

export const counterSlice = createSlice({
    name: 'locations-search-results',
    initialState,
    extraReducers: (builder) => {
        builder.addMatcher(nominatimClient.endpoints.searchBy.matchFulfilled, (state, action) => {
            let places = action.payload.map((item) => {
                return {
                    id: item.place_id,
                    location: item.display_name
                }
            })
            state.places = places
        })
    },
})

export const searchByQuery = useSearchByQuery
export default counterSlice.reducer
