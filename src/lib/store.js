import { configureStore } from "@reduxjs/toolkit"
import locationSearchResultsSlice from "./features/search/location-results"
import selectedLocationSlice from "./features/search/selected-location"
import { nominatimClient } from "./features/search/NominatimClient"

export const store = configureStore({
    reducer: {
        locationResults: locationSearchResultsSlice,
        selectedLocation: selectedLocationSlice,
        [nominatimClient.reducerPath]: nominatimClient.reducer,
    },
    middleware: (getDefultMiddleware) => getDefultMiddleware().concat(nominatimClient.middleware),
})
